//
//  RecipeListViewController.swift
//  FinalMovies
//
//  Created by user180671 on 2/7/21.
//

import UIKit

class RecipeListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var model = BurgerModel()
    var selectedType: BurgerType?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = self
        tableView.delegate = self
        // Do any additional setup after loading the view.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showRecipeDetail" {
            if let indexPath = tableView.indexPathForSelectedRow{
                let burger = model.burgers[indexPath.row]
                let detailVC = segue.destination as? RecipeDetailViewController
                detailVC?.burger = burger
            }
        }
        else if segue.identifier == "addRecipe"{
            let navVC = segue.destination as? UINavigationController
            let addVC = navVC?.viewControllers.first as? AddRecipeViewController
            addVC?.delegate = self
        }
    }
    

    @IBAction func didChangeFilter(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            selectedType = nil
        case 1:
            selectedType = .meat
        case 2:
            selectedType = .vegetarian
        default:
            break
        }
        
        tableView.reloadData()
    }
}
    
extension RecipeListViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.burgers(forType: selectedType).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BurgerCell", for: indexPath )
        let burger = model.burgers(forType: selectedType)[indexPath.row]
        
        cell.textLabel?.text = burger.name
        cell.detailTextLabel?.text = burger.ingredients
        cell.imageView?.image = UIImage(named: burger.thumbnailName)
        
        return cell
    }
}

extension RecipeListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

extension RecipeListViewController: AddRecipeDelegate{
    func add(burger: Burger){
        model.add(burger: burger)
        tableView.reloadData()
    }
}
