//
//  BurgerModel.swift
//  FinalMovies
//
//  Created by user180671 on 2/7/21.
//

import Foundation

class BurgerModel {
    private (set) var burgers: [Burger] = []
    
    init(){
        if let url = Bundle.main.url(forResource: "BurgerResources/burgers", withExtension: "json"){
            do{
                let data = try Data(contentsOf: url)
                burgers = try JSONDecoder().decode([Burger].self, from: data)
            }
            catch{
                print(error)
            }
        }
         
    }
    
    func burgers(forType type: BurgerType?) -> [Burger]{
        guard let type = type else {return burgers}
        return burgers.filter {$0.type == type}
    }
    
    func add(burger: Burger){
        burgers.append(burger)
    }
    
}
