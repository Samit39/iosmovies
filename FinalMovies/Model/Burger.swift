//
//  Burger.swift
//  FinalMovies
//
//  Created by user180671 on 2/7/21.
//

import Foundation

struct Burger: Decodable{
    var name: String
    var ingredients: String
    var imageName: String
    var thumbnailName: String
    var type: BurgerType
}

enum BurgerType: String, Decodable{
    case vegetarian = "vegetarian"
    case meat = "meat"
}
