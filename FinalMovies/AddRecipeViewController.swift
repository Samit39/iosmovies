//
//  AddRecipeViewController.swift
//  FinalMovies
//
//  Created by user180671 on 2/7/21.
//

import UIKit

protocol AddRecipeDelegate {
    func add(burger: Burger)
}

class AddRecipeViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var ingredientsTextView: UITextView!
    
    let ingredientsPlaveholder = "Add ingredients"
    
    var delegate: AddRecipeDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ingredientsTextView.text = ingredientsPlaveholder
        ingredientsTextView.delegate = self
        // Do any additional setup after loading the view.
    }
    

    @IBAction func didTapCancel(_ sender: Any) {
        dismiss(animated: true)
    }
    
    @IBAction func didTapSave(_ sender: Any) {
        guard let name = nameTextField.text else {return}
        let newBurger = Burger(name: name, ingredients: ingredientsTextView.text, imageName: "", thumbnailName: "", type: .vegetarian)
        delegate?.add(burger: newBurger)
        dismiss(animated: true)
    }
}

extension AddRecipeViewController: UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if(textView.textColor == UIColor.tertiaryLabel ){
            textView.text = nil
            textView.textColor = UIColor.label
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if(textView.text.isEmpty){
            textView.text = ingredientsPlaveholder
            textView.textColor = UIColor.tertiaryLabel
        }
    }
}
